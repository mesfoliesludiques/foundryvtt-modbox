Hooks.on("init", () => {
  game.settings.register("modbox", "resetvalue", {
    name: "Reset the modifier value after usage",
    hint: "If set to false the modifier won't be set to 0 after a roll.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });
});

Hooks.on("getSceneControlButtons", (controls) => {
  controls.push({
    name: "modifier",
    title: "CONTROLS.Modifier",
    icon: "fas fa-dice-d20 modbox modifier",
    layer: "TokenLayer",
    tools: [{}],
  });
});

Hooks.on("renderSceneControls", (_, html) => {
  let parent = html.find(".modbox.modifier")[0].parentElement;
  parent.removeAttribute("data-control");
  parent.removeAttribute("data-canvas-layer");
  html.find(".modbox.modifier")[0].parentElement.innerHTML =
    "<input class='modbox modifier-value' type='number' placeholder='0'/>";
});

Hooks.on("preCreateChatMessage", (data) => {
  if (!data.roll) {
    return;
  }
  // Grab toolbar modifier value
  let mod = document.getElementsByClassName("modifier-value")[0];
  if (mod.value == "") return;
  
  // Modify message accordingly
  let parsed = JSON.parse(data.roll);
  parsed.terms.push("+");
  parsed.terms.push(mod.value);
  parsed.formula += ` + ${mod.value}`;
  parsed.total += parseInt(mod.value);

  // Reset the input box
  if (game.settings.get("modbox", "resetvalue")) {
    mod.value = "";
  }

  // Put the data back in the message
  data.roll = JSON.stringify(parsed);
});
